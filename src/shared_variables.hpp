#pragma once
#include <netdb.h>

#define MAX_CHUNK_SIZE 1400

typedef struct {
    struct addrinfo hints;
    struct addrinfo* server_info;
    int sock;
} RAW_SOCKET;

