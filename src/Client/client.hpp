#pragma once
#include "../shared_variables.hpp"

#include <vector>
#include <fstream>

void* get_in_addr(struct sockaddr* sock_addr);
void count_chunks(const char* file_path, size_t* total_chunks, size_t* last_chunk_size);
std::vector<char> make_chunk(std::ifstream & file_stream, size_t total_chunks, size_t last_chunk_size, size_t current_chunk);
std::vector<unsigned char> create_special_header(unsigned packet_number, size_t data_size);
void AES_encryption (const std::vector<char>& data, std::vector<unsigned char>& encrypted_data);
void split_data_to_blocks(std::vector<char> & extracted_data, std::vector<unsigned char> & encrypted_data);
int create_socket(RAW_SOCKET* raw_socket, const char* host_name);
std::string path_to_filename(char* path);
void send_data(RAW_SOCKET* raw_socket, char* file_name);

void send_file_name(RAW_SOCKET* socket, const char* file_name);
void send_final_message(RAW_SOCKET* socket, const char* file_name);
void send_chunk(RAW_SOCKET* socket, std::vector<unsigned char> & data);




