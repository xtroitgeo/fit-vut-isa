/* Algorithm for Efficient Chunked File Reading in C++
 *
 * The MIT License (MIT)
 *
 * Copyright 2014 Andrew Schwartzmeyer
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cstdio>
#include <cstdlib>
#include <netdb.h>	   // getaddrinfo
#include <cstring>	   //memset
#include <arpa/inet.h> // inet_ntop
#include <netinet/ip_icmp.h>
#include <openssl/aes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <iostream>
#include <cerrno>
#include <vector>
#include <fstream>


#include "client.hpp"
#include <poll.h>
#include <sstream>
#include <algorithm>
#include <filesystem>
namespace fs = std::filesystem;


/**
 *  Function creates raw socket for sending data to server
 * @source book: Beej's Guide to Network Programming Using Internet Sockets
 * @param[in] sock_addr - socket structure
 * @return protocol family for socket
 */
void* get_in_addr(struct sockaddr* sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

/**
 * Function make chunk of max 1400 bytes from given file
 * @source https://gist.github.com/andschwa/d120be76ba8ebd66cf50
 *
 * @param[in] file_stream
 * @param[in] total_chunks - number of full chunks by 1400 bytes
 * @param[in] last_chunk_size - size of last chunk < 1400 bytes
 * @param[in] current_chunk - number of current chunk
 * @return current data chunk
 */
std::vector<char> make_chunk(std::ifstream & file_stream, size_t total_chunks, size_t last_chunk_size, size_t current_chunk)
{
    size_t this_chunk_size =
            current_chunk == total_chunks - 1 /* if last chunk */
            ? last_chunk_size /* then fill chunk with remaining bytes */
            : MAX_CHUNK_SIZE; /* else fill entire chunk */

    std::vector<char> chunk_data(this_chunk_size);
    file_stream.read(&chunk_data[0], /* address of buffer start */
                     this_chunk_size); /* this many bytes is to be read */

    return chunk_data;
}

/**
 * Function make chunk of max 1400 bytes from given file
 *
 * @param[in] packet_number - packet number
 * @param[in] data_size - data chunk size
 * @return constructed message header for current packet
 */
std::vector<unsigned char> create_special_header(unsigned packet_number, size_t data_size)
{
    /// Sample of special message header:
    /// EncpX>(size_of_data)DATA
    /// X - packet number
    std::string message;
    message.append("Encp");
    message.append(std::to_string(packet_number));
    message.push_back('>');
    message.append("(").append(std::to_string(data_size)).append(")");
    std::vector<unsigned char> message_header (message.begin(), message.end());
    return message_header;
}

/**
 * Function encrypt data
 *
 * @param[in] data
 * @param[in,out] encrypted_data - vector for storage encrypted data
 */
void AES_encryption(const std::vector<char> & data, std::vector<unsigned char> & encrypted_data)
{
    AES_KEY key_e;

    /// Use encrypt key based on the username
    AES_set_encrypt_key((const unsigned char *)("xtroit00xtroit00"), 128, &key_e);
    unsigned char *output = (unsigned char *)calloc(AES_BLOCK_SIZE,  1);

    AES_encrypt((unsigned char*)data.data(), output, &key_e);
    encrypted_data.insert(encrypted_data.end(), output, output + AES_BLOCK_SIZE);
    free(output);
}

/**
 * Function splits data to block of 16 bytes == AES_BLOCK_SIZE
 *
 * @param[in] extracted_data - data of current chunk
 * @param[in,out] encrypted_data - vector for storage encrypted data
 */
void split_data_to_blocks(std::vector<char> & extracted_data, std::vector<unsigned char> & encrypted_data)
{
    int curr_idx = 0;

    if ( extracted_data.size() > AES_BLOCK_SIZE ){
        std::vector<char> current_block (AES_BLOCK_SIZE, 0);

        for (auto & elem : extracted_data) {
            /// when AES block is full od data -> send it to encrypt
            if ( curr_idx == AES_BLOCK_SIZE ){
                AES_encryption(current_block, encrypted_data);
                for (char & it : current_block) {
                    it = '\0';
                }
                curr_idx = 0;
            }

            current_block[curr_idx] = elem;
            curr_idx++;
        }
        AES_encryption(current_block, encrypted_data);
    }
    else {
        AES_encryption(extracted_data, encrypted_data);
    }
}

/**
 * Function opens file and count number of chunks by 1400 bytes
 * @source https://gist.github.com/andschwa/d120be76ba8ebd66cf50
 *
 * @param[in] file_path
 * @param[in] total_chunks - number of full chunks by 1400 bytes
 * @param[in] last_chunk_size - size of last chunk < 1400 bytes
 */
void count_chunks(const char* file_path, size_t* total_chunks, size_t* last_chunk_size)
{
    std::ifstream file_stream(file_path, std::ifstream::binary);

    if (!file_stream.is_open()){
        perror ("Failed to open file");
        exit(EXIT_FAILURE);
    }

    /// Get file size without seeking to the end and back
    struct stat file_status;
    stat(file_path, &file_status);

    size_t total_size = file_status.st_size;
    size_t chunk_size = MAX_CHUNK_SIZE;

    *total_chunks = total_size / chunk_size;
    *last_chunk_size = total_size % chunk_size;

    /// if the above division was uneven
    if (*last_chunk_size != 0) {
        /// add an unfilled final chunk
        ++*(total_chunks);
    }
        /// if division was even, last chunk is full
    else {
        *last_chunk_size = chunk_size;
    }

    file_stream.close();
}


/**
 * Function send data chunk
 * @source https://gist.github.com/andschwa/d120be76ba8ebd66cf50
 * @source https://beej.us/guide/bgnet/html/#poll
 *
 * @param[in] socket - pointer on raw socket structure
 * @param[in] data - data to send
 */
void send_chunk(RAW_SOCKET* socket, std::vector<unsigned char> & data)
{
    unsigned char packet[1500];
    memset(&packet, 0, 1500);

    struct icmphdr* icmp_header = (struct icmphdr*) packet;
    icmp_header->code = ICMP_ECHO;
    icmp_header->checksum = 0;

    memcpy(packet + sizeof(struct icmphdr), data.data(), data.size());

    usleep(10);

    struct pollfd pfds[1];
    pfds[0].fd = socket->sock;
    pfds[0].events = POLLOUT;

//    printf("Hit RETURN or wait 2,5 second for timeout\n");

    int num_events = poll(pfds, 1, 2500);

    if (num_events == 0) {
        printf("Poll timed out!\n");
    } else {
        int pollout_happened = pfds[0].revents & POLLOUT;

        if (pollout_happened) {
            if (pfds[0].fd == socket->sock) {
                if (sendto(socket->sock, packet, sizeof(struct icmphdr) + data.size(), 0,
                           (struct sockaddr *) (socket->server_info->ai_addr), socket->server_info->ai_addrlen) < 0) {

                    perror("sendto() failed");
                    exit(EXIT_FAILURE);
                }
            }
        } else {
            printf("Unexpected event occurred: %d\n", pfds[0].revents);
        }
    }

}
/**
 *  Function send file to server
 *
 * @param[in] socket - structure RAW_SOCKET
 * @param[in] file_name - file name to send
 */
void send_file_name(RAW_SOCKET* socket, const char* file_name)
{
    std::string message_header;
    size_t data_size = strlen(file_name);
    message_header.append("Encp1>");
    message_header.append("(").append(std::to_string(data_size)).append(")").append(file_name);
    printf("File name = %s\n", message_header.data());

    std::vector<char> first_message (message_header.begin(), message_header.end());

    std::vector<unsigned char> encrypted_data;

    split_data_to_blocks(first_message, encrypted_data);
    send_chunk(socket, encrypted_data);
}

/**
 *  Function send file to server
 *
 * @param[in] socket - structure RAW_SOCKET
 * @param[in] file_name - file name to send
 */
void send_final_message(RAW_SOCKET* socket, const char* file_name)
{
    std::string message;
    size_t data_size = strlen(file_name) + 5 + 23;
    message.append("Encp").append(">");
    message.append("(").append(std::to_string(data_size)).append(")");
    message.append("File ").append(file_name).append(" was successfully sent.");

    std::vector<char> final_message (message.begin(), message.end());
    std::vector<unsigned char> encrypted_data;

    split_data_to_blocks(final_message, encrypted_data);

    send_chunk(socket, encrypted_data);
}

/**
 * Function extract filename from path
 * @param[in] path
 * @return filename without path
 */
std::string path_to_filename (char* path)
{
    std::stringstream string_stream;
    string_stream << fs::path(path).filename();
    std::string filename = string_stream.str();
    filename.erase(std::remove( filename.begin(), filename.end(), '\"' ),filename.end());
    return filename;
}

/**
 *  Function send file to server
 *
 * @param[in] raw_socket - structure RAW_SOCKET
 * @param[in] file_name - file to send
 */
void send_data(RAW_SOCKET* raw_socket, char* file_name)
{

    size_t total_chunks;
    size_t last_chunk_size;
    std::ifstream file_stream(file_name, std::ifstream::binary);

    count_chunks(file_name, &total_chunks, &last_chunk_size);

    std::vector<unsigned char> message_header;
    std::vector<char> extracted_data;
    std::vector<unsigned char> encrypted_data;

    std::string temp = path_to_filename(file_name);

    for (int i = 0; temp[i] != '\0'; ++i) {
        file_name[i] = temp[i];
    }
    file_name[temp.size()] = '\0';

    /// Send 1st packet with file name
    send_file_name(raw_socket, file_name);

    for (size_t curr_chunk = 0; curr_chunk < total_chunks; ++curr_chunk) {

        /// Make chunk of size max 1400 bytes
        extracted_data = make_chunk(file_stream, total_chunks, last_chunk_size, curr_chunk);
        /// Add special header before data
        message_header = create_special_header(curr_chunk + 2, extracted_data.size());
        extracted_data.insert(extracted_data.begin(), message_header.begin(), message_header.end());

        /// Split data to blocks of 16 bytes
        /// Encrypt this blocks
        split_data_to_blocks(extracted_data, encrypted_data);
        /// Send current encrypted chunk
        send_chunk(raw_socket, encrypted_data);
        encrypted_data.clear();
    }
    file_stream.close();
    send_final_message(raw_socket, file_name);
    printf("Client send file %s to server\n", file_name);
}



/**
 *  Function creates raw socket for sending data to server
 *
 * @param[in] raw_socket - structure RAW_SOCKET
 * @param[in] host_name - hostname of server
 * @return    -1 in case of function error, 1 if socket was create successful
 */
int create_socket(RAW_SOCKET* raw_socket, const char* host_name){

    int protocol;
    int result;
    char ip[100];

    memset(&raw_socket->hints, 0, sizeof(raw_socket->hints));

    raw_socket->hints.ai_family = AF_UNSPEC;
    raw_socket->hints.ai_socktype = SOCK_RAW;

    if ((result = getaddrinfo(host_name, nullptr, &raw_socket->hints, &(raw_socket->server_info))) != 0){
        perror(gai_strerror(result));
        return -1;
    }

    inet_ntop(raw_socket->server_info->ai_family, get_in_addr(raw_socket->server_info->ai_addr), ip, 100);
//    printf("IP: %s\n", ip);

    /// Choose protocol based on IP version
    if (raw_socket->server_info->ai_family == AF_INET) {
        protocol = IPPROTO_ICMP;
    }
    else {
        protocol = IPPROTO_ICMPV6;
    }

//    printf("Socket type: %d\n",raw_socket->server_info->ai_socktype);

    raw_socket->sock = socket(raw_socket->server_info->ai_family, raw_socket->server_info->ai_socktype, protocol);
    if (raw_socket->sock == -1) {
        perror ("Failed to create socket");
        return -1;
    }

//    freeaddrinfo(raw_socket->server_info);

    return 1;
}


