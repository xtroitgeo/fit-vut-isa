#include <cstdio>
#include <cstdlib>
#include <unistd.h> // getopt
#include <cstring>

#include "Client/client.hpp"
#include "Server/server.hpp"
#include "shared_variables.hpp" // struct RAW_SOCKET, macros


void free_resources(char* file_path, char* hostname)
{
    if (file_path != nullptr){
        free(file_path);
    }
    if (hostname != nullptr){
        free(hostname);
    }
}

/**
 * Function checks, what arguments were entered
 * @source based on example from man page getopt
 */
void parse_params(int argc, char* argv[], char*& file_path, char*& hostname, bool & enter_file_name, bool & enter_host_name, bool & server_mode)
{
    int option;

    while ((option = getopt(argc, argv, "r:s:l")) != -1) {
        if (server_mode){
            return;
        }
        switch (option) {
            case 'r': { // arg for file name
                enter_file_name = true;

                int file_path_length = strlen(optarg) + 1;
                file_path = (char*) malloc (file_path_length);
                memcpy(file_path, optarg, file_path_length);

                if (file_path[0] == '-'){
                    if (file_path_length >=3 && file_path[1] == 'l'){
                        server_mode = true;
                        return;
                    }
                    free_resources(file_path, hostname);
                    file_path = nullptr;
                    hostname = nullptr;
                }
                break;
            }
            case 's': { // arg for ip or hostname
                enter_host_name = true;

                int hostname_length = strlen(optarg) + 1;
                hostname = (char*) malloc (hostname_length);
                memcpy(hostname, optarg, hostname_length);

                if (hostname[0] == '-'){
                    if (hostname_length >=3 && hostname[1] == 'l'){
                        server_mode = true;
                        return;
                    }
                    free_resources(file_path, hostname);
                    hostname = nullptr;
                    file_path = nullptr;
                }
                break;
            }
            case 'l': { // arg for server mode
                printf("Server mode\n");
                server_mode = true;
                break;
            }

            default: { // '?'
                if (isprint (optopt)) {
                    fprintf(stderr, "Usage: %s -r <file> -s <ip|hostname> [-l]\n", argv[0]);
                }
                return;
            }
        }
    }
}


int main(int argc, char *argv[]) {

    bool server_mode = false;
    bool enter_file_name = false;
    bool enter_host_name = false;

    char* file_path = nullptr;
    char* hostname = nullptr;
    RAW_SOCKET raw_socket;

    parse_params(argc, argv, file_path, hostname, enter_file_name, enter_host_name, server_mode);

    if (server_mode){
        run_server();
    }
    else {
        /// In client mode program must have two mandatory arguments with options
        if (!enter_host_name || !enter_file_name){
            free_resources(file_path, hostname);
            fprintf(stderr,"Arguments -r and -s require options\n");
            return 1;
        }

        if ( create_socket(&raw_socket, hostname) == -1 ){
            free_resources(file_path, hostname);
            return 1;
        }
        send_data(&raw_socket, file_path);
    }

    free_resources(file_path, hostname);

    return 0;
}
