#include <netdb.h>	   // getaddrinfo
#include <cstring>	   //memset
#include <arpa/inet.h> // inet_ntop
#include <netinet/ip_icmp.h>
#include <openssl/aes.h>
#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>

#define  MAX_PACKET_SIZE 1400
#define IPv4_HDRLEN 20
#define IPv6_HDRLEN 40
#define ICMP_HDRLEN 8


/// RUN sudo ./transfer

void* get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET)
	{
		return &(((struct sockaddr_in *)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

int get_length(const char* buffer)
{
    int buffer_size = 0;
    for (int i = 0; i != MAX_PACKET_SIZE && buffer[i] != '\0'; ++i) {
        buffer_size++;
    }
    return buffer_size;
}

// Computing the internet checksum (RFC 1071).
// Note that the internet checksum does not preclude collisions.
/// @source https://www.cs.dartmouth.edu/~sergey/cs60/lab3/icmp4-rawsend.c
uint16_t checksum (uint16_t *addr, int len)
{
    int count = len;
    uint32_t sum = 0;
    uint16_t answer = 0;

    // Sum up 2-byte values until none or only one byte left.
    while (count > 1) {
        sum += *(addr++);
        count -= 2;
    }

    // Add left-over byte, if any.
    if (count > 0) {
        sum += *(uint8_t *) addr;
    }

    // Fold 32-bit sum into 16 bits; we lose information by doing this,
    // increasing the chances of a collision.
    // sum = (lower 16 bits) + (upper 16 bits shifted right 16 bits)
    while (sum >> 16) {
        sum = (sum & 0xffff) + (sum >> 16);
    }

    // Checksum is one's compliment of sum.
    answer = ~sum;

    return (answer);
}

void AES (unsigned char* cyphertext, int cyphertextlen)
{

    printf("Text length: %d\n", cyphertextlen);
    for (int i = 0; i < cyphertextlen; ++i) {
        printf("%c", cyphertext[i]);
    }
    printf("\n");

    AES_KEY key_e;
    AES_KEY key_d;
    const unsigned char key [] = "xtroit00";
    AES_set_encrypt_key(key, 128, &key_e);
    AES_set_decrypt_key(key, 128, &key_d);

    unsigned char *output = (unsigned char *)calloc(AES_BLOCK_SIZE,  1);

    printf("Nejaky vypis:\n");
    for (int i = 0; i < AES_BLOCK_SIZE; ++i) {
        printf("%d)%c|", i, output[i]);
    }
    printf("\n");

    AES_encrypt(cyphertext, output, &key_e);

    printf("encrypted: ");
    for (int i = 0; i < AES_BLOCK_SIZE; ++i)
    {
        printf("%X ", output[i]);
    }
    printf("\n");

    AES_decrypt(output, output, &key_d);

    printf("After decryption: ");
    for (int i = 0; i < AES_BLOCK_SIZE; ++i)
    {
        printf("%X ", output[i]);
    }

//    printf("\ndecrypted: %s\n", output);
    printf("\n");
    for (int i = 0; i < AES_BLOCK_SIZE && output[i] != '\0'; ++i) {
        printf("%d: %c\n", i, output[i]);
    }

    printf("KONEC SIFROVANI\n");

    free(output);
}

int func(){
    /// Sifrovani
    int cyphertextlen = 3;
    int curr_idx = 0;
    unsigned char cyphertext[] = "Vyp";

    printf("Before encrypting: ");
    for (int i = 0; i < cyphertextlen; ++i) {
        printf("%X ", cyphertext[i]);
    }
    printf("\n");

    if ( cyphertextlen > AES_BLOCK_SIZE ){
        unsigned char current_block [AES_BLOCK_SIZE];

        for (int i = 0; cyphertext[i] != '\0'; ++i) {
            if ( curr_idx == AES_BLOCK_SIZE ){
                AES(current_block, curr_idx);
                memset(&current_block, 0, AES_BLOCK_SIZE);
                curr_idx = 0;
            }
            current_block[curr_idx] = cyphertext[i];
            curr_idx++;
        }
        AES(current_block, curr_idx);
    }
    else {
        AES(cyphertext, cyphertextlen);
    }

    return 0;
}


int main(int argc, char **argv)
{
	struct addrinfo hints, *serverinfo;
	memset(&hints, 0, sizeof(hints));

	char host[] = "localhost";
	int result;

	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_RAW;

	if ((result = getaddrinfo(host, nullptr, &hints, &serverinfo)) != 0)
	{
		fprintf(stderr, "%s\n", gai_strerror(result));
		return 1;
	}

    printf("SERVER AI_FAMILY: %d\n", serverinfo->ai_family);
    printf("SERVER SA_FAMILY: %d\n", serverinfo->ai_addr->sa_family);

	char ip[100];
	inet_ntop(serverinfo->ai_family, get_in_addr(serverinfo->ai_addr), ip, 100);
	printf("ip: %s\n", ip);

    int protocol;

    if (serverinfo->ai_family == AF_INET) {
        protocol = IPPROTO_ICMP;
    }
    else {
        protocol = IPPROTO_ICMPV6;
    }

    printf("SOCKTYPE: %d\n",serverinfo->ai_socktype);

	int sock = socket(serverinfo->ai_family, serverinfo->ai_socktype, protocol);
	if (sock == -1)
	{
		fprintf(stderr, "sock err :)\n");
		return 1;
	}

    /// ******************************************************* ///
    //// SENDING FILE ////
//
//    std::ifstream fileStream;
//    fileStream.open("example", std::ios::in | std::ios::binary);
//    std::streampos file_size = 0;
//
//    int iter = 1;
//    char packet[1500];
//    memset(&packet, 0, 1500);
//
//    struct icmphdr *icmp_header = (struct icmphdr *)packet;
//    icmp_header->code = ICMP_ECHO;
//    icmp_header->checksum = 0;     /// Checksum
//
//    if (fileStream.is_open()){
//        FILE *fw = fopen("file_to_send", "w");
//        char* data = (char*) malloc (MAX_PACKET_SIZE);
//
//        file_size = fileStream.tellg();
//        fileStream.seekg(0, std::ios::end);
//        file_size = fileStream.tellg() - file_size;
//
//        long int loops = file_size / MAX_PACKET_SIZE;
//        long int lastChunk = file_size % MAX_PACKET_SIZE;
//
//        printf("Loops=%ld; Chunks = %ld\n", loops, lastChunk);
//
//
//
//        for (int i = 0; i < loops; i++) {
//            fileStream.read(data, MAX_PACKET_SIZE);
//            int dataLen = get_length(data);
//            printf("Data len = %d\n",dataLen);
//
//            memcpy(packet + sizeof(struct icmphdr), data), dataLen);
//
//            if (sendto(sock, packet, sizeof(struct icmphdr) + dataLen, 0, (struct sockaddr *)(serverinfo->ai_addr), serverinfo->ai_addrlen) < 0){
//
//                fprintf(stderr, "sendto error\n");
//                return 1;
//            }
//
//            memset(&packet, 0, 1500);
//        }
//        if (lastChunk > 0) {
//                fileStream.read(data, lastChunk);
//                int dataLen = get_length(data);
//                memcpy(packet + sizeof(struct icmphdr), data, dataLen);
//
//                if (sendto(sock, packet, sizeof(struct icmphdr) + dataLen, 0, (struct sockaddr *)(serverinfo->ai_addr), serverinfo->ai_addrlen) < 0) {
//                    fprintf(stderr, "sendto error\n");
//                    return 1;
//                }
//
//                memset(&packet, 0, 1500);
//        }
//       fclose(fw);
//       free(data);
//    }

//	char packet[1500];
//	char data[] = "Czech";
//	int dataLen = 5;
//
//	///  ICMP packet size = 28 	 (DATA_MAX = 1472)
//	///  ICMPv6 packet size = 48 (DATA_MAX = 1452)
//
//	memset(&packet, 0, 1500);
//
//	struct icmphdr *icmp_header = (struct icmphdr *)packet;
//	icmp_header->code = ICMP_ECHO;
//	icmp_header->checksum = 0;     /// Checksum
//
//	memcpy(packet + ICMP_HDRLEN, data, dataLen);
//
//    //	printf("Delka hlavicky: %lu\n", sizeof(struct icmphdr));
//
//	if (sendto(sock, packet, sizeof(struct icmphdr) + dataLen, 0, (struct sockaddr *)(serverinfo->ai_addr), serverinfo->ai_addrlen) < 0)
//	{
//		fprintf(stderr, "sendto error\n");
//		return 1;
//	}

//	 /// Sifrovani
//    int cyphertextlen = 3;
//    int curr_idx = 0;
//	unsigned char cyphertext[] = "Vyp";
//
//    printf("Before encrypting: ");
//    for (int i = 0; i < cyphertextlen; ++i) {
//        printf("%X ", cyphertext[i]);
//    }
//    printf("\n");
//
//    if ( cyphertextlen > AES_BLOCK_SIZE ){
//        unsigned char current_block [AES_BLOCK_SIZE];
//
//        for (int i = 0; cyphertext[i] != '\0'; ++i) {
//            if ( curr_idx == AES_BLOCK_SIZE ){
//                AES_encryption(current_block, curr_idx);
//                memset(&current_block, 0, AES_BLOCK_SIZE);
//                curr_idx = 0;
//            }
//            current_block[curr_idx] = cyphertext[i];
//            curr_idx++;
//        }
//        AES_encryption(current_block, curr_idx);
//    }
//    else {
//        AES_encryption(cyphertext, cyphertextlen);
//    }
//
//    freeaddrinfo(serverinfo);
//    return 0;
}







