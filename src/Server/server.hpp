#pragma once

#include <pcap.h>
#include <pcap/pcap.h>
#include <filesystem>
namespace fs = std::filesystem;

int cnt_bytes_to_read(unsigned char* decrypted_block, int idx, int chunk_size);
void AES_decryption(const std::vector<unsigned char> & data, std::vector<unsigned char> & decrypted_data, size_t chunk_size, bool is_last_block);
void split_data_to_blocks(const std::vector<unsigned char> & data, std::vector<unsigned char> & decrypted_data);
void extract_data_from_packet(std::vector<unsigned char> & data, const unsigned char* icmp_data, unsigned int data_length);
void extract_file_name(std::vector<unsigned char> & decrypted_data, char* file_name);
bool is_last_message(std::vector<unsigned char > & decrypted_data, char* file_name);
void save_data_to_file(char* file_name, std::vector<unsigned char> & decrypted_data);
void decrypt_and_save_data(std::vector<unsigned char> & data);
void mypcap_handler(u_char *args, const struct pcap_pkthdr *header, const u_char *packet);
int run_server();




