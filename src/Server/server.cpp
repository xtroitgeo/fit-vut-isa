/*
 * The code base for the program was taken from the examples in the WIS system of the ISA subject.
 * @source examples/pcap/sniff-filter.c
 *
 * Czech Republic, VUT FIT
 */
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <pcap.h>
#include <pcap/pcap.h>
#include <cerrno>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>

#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <err.h>

#include <openssl/aes.h>

#ifdef __linux__            // for Linux
#include <netinet/ether.h>
#include <ctime>
#include <pcap/pcap.h>
#endif

#ifndef PCAP_ERRBUF_SIZE
#define PCAP_ERRBUF_SIZE (256)
#endif

#define SIZE_ETHERNET (14)       // offset of Ethernet header to L3 protocol

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <csignal>

#include <filesystem>
#include <fstream>
#include <algorithm>

namespace fs = std::filesystem;


/// Structure for storage payload
typedef struct{
    unsigned char* data;
    unsigned int chunk_length;
    unsigned int decrypted_data_length;
} Payload;

/**
 * Function count number of necessary bytes in last data block (in current chunk)
 * @param[in] decrypted_block - decrypted block
 * @param[in] idx - index on element after message header "Encp"
 * @param[in] chunk_size - payload size
 *
 * @return number of bytes to read from last block
 */
int cnt_bytes_to_read(unsigned char* decrypted_block, int idx, unsigned int chunk_size){

    int bytes_to_read_from_last_chunk = 0;
    int message_hdr_length = 0;

    while(idx < AES_BLOCK_SIZE && decrypted_block[idx] != '>'){
        idx++;
    }

    /// extract actual data size (size before encryption and padding)
    /// it's in parentheses after message header
    std::vector<unsigned char> temp;
    int origin_chunk_length = 0;
    if (idx + 1 < AES_BLOCK_SIZE && decrypted_block[idx + 1] == '('){
        idx += 2;
        for (;idx < AES_BLOCK_SIZE && decrypted_block[idx] != ')'; ++idx) {
            temp.push_back(decrypted_block[idx]);
        }
    }
    message_hdr_length = idx;
    if (idx < AES_BLOCK_SIZE && decrypted_block[idx] == ')') {
        std::string extracted_number_by_chars (temp.begin(), temp.end());
        std::stringstream str(extracted_number_by_chars);
        str >> origin_chunk_length;

        if ( origin_chunk_length != 0 ){
            bytes_to_read_from_last_chunk = chunk_size - origin_chunk_length - message_hdr_length;
            bytes_to_read_from_last_chunk = AES_BLOCK_SIZE - bytes_to_read_from_last_chunk;
        }
    }
    else {
        bytes_to_read_from_last_chunk = AES_BLOCK_SIZE;
    }

    return bytes_to_read_from_last_chunk;
}


/**
 * Function decrypt data
 * overwrites data of 16 byte in payload
 * @param[in] data - current block
 * @param[in] chunk - structure Payload
 * @param[in] is_last_block
 */
void AES_decryption(const std::vector<unsigned char> & data, Payload* chunk, bool is_last_block)
{
    AES_KEY key_d;
    /// Use predefined key based on student username
    AES_set_decrypt_key((unsigned char*)"xtroit00xtroit00", 128, &key_d);

    unsigned char *output = (unsigned char *)calloc(AES_BLOCK_SIZE,  1);
    AES_decrypt((unsigned char*)data.data(), output, &key_d);

    unsigned char start_of_message[5];
    size_t idx = 0;

    /// Control special string "Encp" to identify right packet
    for (; idx < 4; idx++){
        start_of_message[idx] = output[idx];
    }
    start_of_message[idx] = '\0';

    int bytes_to_read = AES_BLOCK_SIZE;
    static int bytes_to_read_from_last_chunk;

    /// Extract all information about current chunk
    /// Info can be after message header "EncpX>"
    if ( strcmp((char*)start_of_message, "Encp") == 0 ){
        bytes_to_read_from_last_chunk = cnt_bytes_to_read(output, idx, chunk->chunk_length);
    }

    /// last block can have less than 16 bytes of necessary data
    if (is_last_block){
        bytes_to_read = bytes_to_read_from_last_chunk + 1;
    }

    if (bytes_to_read > AES_BLOCK_SIZE){
        bytes_to_read = AES_BLOCK_SIZE;
    }

    for (int i = 0; i < bytes_to_read; ++i) {
        chunk->data[chunk->decrypted_data_length] = output[i];
        chunk->decrypted_data_length++;
    }

    free(output);
}

/**
 * Function split data to blocks of 16 bytes
 * Send it to decrypt
 * @param[in] chunk - structure Payload (payload + lengths)
 */
void split_data_to_blocks(Payload* chunk)
{
    /// count full AES blocks in current chunk (packet)
    int aes_blocks_in_chunk = chunk->chunk_length / AES_BLOCK_SIZE;
    int start = 0;
    int end = 0;
    bool is_last_block = false;
    int curr_idx = 0;

    /// send each AES block to decrypt function
    for (int i = 1; i <= aes_blocks_in_chunk; i++){
        std::vector<unsigned char> current_block (AES_BLOCK_SIZE, 0);
        start = (i-1) * AES_BLOCK_SIZE;
        end = i * AES_BLOCK_SIZE;

        for (int idx = start; idx < end; idx++){
            current_block[curr_idx] = chunk->data[idx];
            curr_idx++;
        }
        if (i == aes_blocks_in_chunk){
            is_last_block = true;
        }
        AES_decryption(current_block, chunk, is_last_block);
        current_block.clear();
        curr_idx = 0;
    }

}


/**
 * Function create file
 * @param[in] file_name - file where the data is written to
 * @param[in] chunk - structure Payload (has decrypted data)
 */
void extract_file_name(Payload* chunk, char* file_name)
{
    FILE *fw;
    int file_name_length = 0;
    int start;

    /// Find out, what filename length we have
    int n_digit_number = 0;
    int idx = 7;
    while ( chunk->data[idx] != ')'){
        n_digit_number++;
        idx++;
    }

    if (n_digit_number == 1){
        start = 9;
    }
    else {
        start = 10;
    }

    for (size_t i = start; i < chunk->decrypted_data_length; i++) {
        file_name[file_name_length] = chunk->data[i];
        file_name_length++;
    }
    file_name[file_name_length] = '\0';

    /// Extract filename from absolute path of file
    fw = fopen(file_name, "ab");
    fclose(fw);
}

/**
 * Function check if was send final accept message
 * @param[in] file_name - file where the data is written to
 * @param[in] chunk - structure Payload (has decrypted data)
 */
bool is_last_message(Payload* chunk, char* file_name)
{
    /// Reference last message
    std::string reference;
    reference.append("File ").append(file_name).append(" was successfully sent.");

    std::string message;
    std::vector<unsigned char> temp;
    int message_length = 0;
    int idx = 6;

    while (chunk->data[idx] != ')'){
        temp.push_back(chunk->data[idx]);
        idx++;
    }
    /// extract filename length from message
    std::string extracted_number_by_chars (temp.begin(), temp.end());
    std::stringstream str(extracted_number_by_chars);
    str >> message_length;

    idx++;
    for (int i = idx; i < message_length + idx; i++){
        message.push_back(chunk->data[i]);
    }

    if (reference != message) {
        return false;
    }

    return true;
}

/**
 * Function save decrypted data to file
 * @param[in] file_name - file where the data is written to
 * @param[in] decrypted_data - structure Payload (with decrypted data)
 */
void save_data_to_file(char* file_name, Payload* decrypted_data)
{
    FILE *fw;

    /// Extract filename from absolute path of file
    fw = fopen(file_name, "ab");

    if (fw == nullptr){
        perror("Error occurred while opening file.");
        exit(1);
    }

    /// Skip additional packet info
    int idx = 4;
    while(decrypted_data->data[idx] != '>'){
        idx++;
    }

    if (decrypted_data->data[idx + 1] == '('){
        idx++;
        while(decrypted_data->data[idx] != ')'){
            idx++;
        }
    }

    for (unsigned int i = idx + 1; i < decrypted_data->decrypted_data_length; i++){
        fprintf(fw, "%c", decrypted_data->data[i]);
    }
    fclose(fw);
}

/**
 * Function decrypt data, and verify it
 * Save it to file
 * @param[in] icmp_payload - vector for data
 */
void decrypt_and_save_data(unsigned char* icmp_payload, unsigned int data_length)
{

    Payload chunk;
    chunk.data = icmp_payload;
    chunk.chunk_length = data_length;
    chunk.decrypted_data_length = 0;

    /// Split data to blocks of 16 bytes and decrypt
    split_data_to_blocks(&chunk);

    unsigned char start_of_message[5];
    /// Control special string "Encp" to identify right packet
    for (int i = 0; i < 4; i++){
        start_of_message[i] = icmp_payload[i];
    }
    start_of_message[4] = '\0';

    if ( strcmp((char*)start_of_message, "Encp") != 0 ){
        return;
    }

    static char file_name[50];
    int packet_number = icmp_payload[4] - '0';

    if (packet_number == 1 && chunk.data[5] == '>'){
        extract_file_name(&chunk, file_name);
        return;  /// return, this packet sniffing process ends
    }

    /// Last packet doesn't have packet number
    if ( chunk.data[4] == '>'){
        if (is_last_message(&chunk, file_name)){
            printf("File %s was fully accepted\n", file_name);
            printf("====================================================\n");
            return;
        }
    }

    save_data_to_file(file_name, &chunk);
}


/**
 *  mypcap_handler() is a function that processes captured packets
 *  the function is called by pcap_loop()
 * @source WIS examples
 * @param[in] header
 * @param[in] packet
 */
void mypcap_handler(u_char *args, const struct pcap_pkthdr *header, const u_char *packet)
{
    struct ip *ipv4_header;                // pointer to the beginning of IPv4 header
    struct ip6_hdr *ipv6_header;           // pointer to the beginning of IPv6 header
    struct ether_header *ethernet_header;  //  pointer to the beginning of Ethernet header
    const struct icmphdr *my_icmp;         //  pointer to the beggining of ICMP header
    unsigned int size_ip4 = 0;
    unsigned int size_ip6 = 0;
    size_ip6 += sizeof(struct ether_header);

    ethernet_header = (struct ether_header *) packet;

    switch (ntohs(ethernet_header->ether_type)){
        case ETHERTYPE_IP:{ // IPv4 packet
            /// skip Ethernet header
            ipv4_header = (struct ip*) (packet + sizeof(struct ether_header));
            /// length of IP header
            size_ip4 = ipv4_header->ip_hl * 4;

            /// get payload from icmp packet
            /// @source https://stackoverflow.com/questions/22090396/how-to-copy-message-inside-icmp-header
            if ( ipv4_header->ip_p == IPPROTO_ICMP ){

                my_icmp = (struct icmphdr *) (packet + SIZE_ETHERNET + size_ip4); /// pointer to the ICMP header
                /// Pointer to payload
                unsigned char* icmp_data = (unsigned char *)((unsigned char *)my_icmp + sizeof(struct icmphdr *));
                //// Size of payload
                unsigned int data_length = header->len - ( SIZE_ETHERNET + size_ip4 + sizeof(struct icmphdr *));

                /// Check, that data has the special message_header
                /// At first, check length
                if (data_length < 6){
                    break;
                }
                decrypt_and_save_data(icmp_data, data_length);
            }
            break;
        }


        case ETHERTYPE_IPV6:{  // IPv6 packet

            /// Get ipv6 header
            ipv6_header = (struct ip6_hdr*)(packet + size_ip6);

            /// Go to next header in ipv6
            int next_header = ipv6_header->ip6_nxt;

            /// length of IPv6 header
            size_ip6 += sizeof(struct ip6_hdr);

            if (next_header == IPPROTO_ICMPV6){

                /// Pointer to payload
                unsigned char* icmp6_data = (unsigned char *)(packet + size_ip6 + sizeof(struct icmp6_hdr));
                //// Size of payload
                unsigned int data_length = header->len - size_ip6 - sizeof(struct icmp6_hdr);

                /// Check, that data has the special message_header
                /// At first, check length
                if (data_length < 6){
                    break;
                }

                decrypt_and_save_data(icmp6_data, data_length);
            }
            break;
        }
    }
}

void signalHandler( int signum ) {
    std::cout << "Interrupt signal (" << signum << ") received.\n";
    exit(signum);
}

/**
 *  Sniffer icmp packets
 * @source WIS examples
 * @param[in] sock_addr - socket structure
 */
int run_server() {
    char errbuf[PCAP_ERRBUF_SIZE] = {0};  // constant defined in pcap.h
    pcap_t *handle;                 // packet capture handle
    pcap_if_t *alldev;              // a list of all input devices
    char *devname;                  // a name of the device
    struct in_addr a, b;
    bpf_u_int32 netaddr;            // network address configured at the input device
    bpf_u_int32 mask;               // network mask of the input device
    struct bpf_program fp;          // the compiled filter

    signal(SIGINT, signalHandler);

    /// open the input devices (interfaces) to sniff data
    if (pcap_findalldevs(&alldev, errbuf) != 0) {
        printf("error: %s\n", errbuf);
        exit(-1);
    }

    printf("\n");


    /// select the name of first interface (default) for sniffing
//    devname = alldev->name;

    /// select next interface to sniff loopback
    devname = alldev->next->name;

    /// get IP address and mask of the sniffing interface
    if (pcap_lookupnet(devname, &netaddr, &mask, errbuf) == -1) {
        err(1, "pcap_lookupnet() failed");
    }

    a.s_addr = netaddr;
    printf("Opening interface \"%s\" with net address %s,", devname, inet_ntoa(a));
    b.s_addr = mask;
    printf("mask %s for listening...\n", inet_ntoa(b));


    /// open the interface for live sniffing
    if ((handle = pcap_open_live(devname, BUFSIZ, 1, 1000, errbuf)) == nullptr) {
        err(1, "pcap_open_live() failed");
    }


    /// compile the filter for capturing only ICMP
    if (pcap_compile(handle, &fp, "icmp or icmp6", 0, netaddr) == -1) {
        err(1, "pcap_compile() failed");
    }

    /// /set the filter to the packet capture handle
    if (pcap_setfilter(handle, &fp) == -1) {
        err(1, "pcap_setfilter() failed");
    }


    /// read packets from the interface in the infinite loop (count == -1)
    /// incoming packets are processed by function mypcap_handler()
    if (pcap_loop(handle, -1, mypcap_handler, nullptr) != -1) {
        err(1, "pcap_loop() failed");
    }

    /// close the capture device and deallocate resources
    pcap_close(handle);
    pcap_freealldevs(alldev);
    return 0;
}




