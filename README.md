# ISA - Network applications and network management
# Semester project 
[Assignment](assignment.pdf)  in Czech.

### Task details
- Application transfers file inside ICMP Echo-Request messages
- The file is encrypted with AES cipher before transmission
- The program, running in listen (-l) mode, intercepts these messages, decrypts them and saves the file to disk

### Run:
./secret -r file -s <ip|hostname> [-l] \
-r file : file specification for transfer \
-s <ip|hostname> : destination ip address/hostname \
-l : if the program is run with this parameter, it's a server that listens for incoming ICMP messages and stores the file in the same directory where it was run

#### Man page
man ./src/secret.1


## Evaluation 20/20

